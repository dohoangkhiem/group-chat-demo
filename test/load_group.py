from socketIO_client import SocketIO, LoggingNamespace
import thread
import threading

token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6MiwidXNlcm5hbWUiOiJraGllbSIsImVtYWlsIjoiZG9ob2FuZ2toaWVtQGdtYWlsLmNvbSIsImRpc3BsYXlOYW1lIjoiS2hpZW0iLCJwYXNzd29yZCI6ImEiLCJpYXQiOjE0MzEzMjI2NTAsImV4cCI6MTQzMTM0MDY1MH0.PsJZDeXHU9T3XPaTksPNufBT_9cHyu2tEo6UViuVc2U'

def on_error(*args):
  print('Error %s' % args)


def on_msg(*args):
  print('Message: %s' % args)


def group_ready(*args):
  print 'Group ready %s' % args


def thread_1(thr_name):
    global token
    with SocketIO('localhost', 3000, params={'token': token}) as socketIO:
        # login
        socketIO.emit('login', {'id': 2, 'username': 'khiem'})

        socketIO.on('group-ready', group_ready)

        socketIO.on('chat-message', on_msg)

        socketIO.wait(seconds=5)
        
        # load a group
        socketIO.emit('load-group', 6)

        socketIO.wait(seconds=10000000)


def thread_2(thr_name):
    global token
    with SocketIO('localhost', 3000, params={'token': token}) as socketIO:
        socketIO.emit('login', {'id': 6, 'username': 'selena'})

        socketIO.on('group-ready', group_ready)  

        socketIO.on('chat-message', on_msg)

        socketIO.wait(seconds=10000000)


def thread_3(thr_name):
    global token
    with SocketIO('localhost', 3000, params={'token': token}) as socketIO:
        socketIO.emit('login', {'id': 1, 'username': 'admin'})

        socketIO.on('group-ready', group_ready)  

        socketIO.on('chat-message', on_msg)

        socketIO.wait(seconds=10000000)


thread.start_new_thread(thread_1, ('thread-1',))
thread.start_new_thread(thread_2, ('thread-2',))
thread.start_new_thread(thread_3, ('thread-3',))

while 1:
  pass